package com.alemdianov.cucumber.steps;

import com.alemdianov.api.services.PetServiceSteps;
import io.cucumber.java.Before;
import io.cucumber.java8.En;
import io.swagger.client.model.ModelApiResponse;
import io.swagger.client.model.Pet;
import io.swagger.client.model.Tag;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.SoftAssertions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.swagger.client.model.Pet.StatusEnum.PENDING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;

public class PetStoreStepsDefinitions implements En {

    private String petName;
    private String updatedPetName;
    private SoftAssertions softly;
    private Pet pet;
    private Pet createdPet;
    private int statusCode;
    private Pet[] foundPets;
    private List<Tag> newTags;
    private List<Tag> updatedTags;
    private String message;

    @Steps
    private PetServiceSteps petServiceSteps;

    @Before
    public void setTheStage() {

        OnStage.setTheStage(new OnlineCast());
        petServiceSteps = new PetServiceSteps();
        petName = UUID.randomUUID().toString();
        softly = new SoftAssertions();
        pet = new Pet()
                .name(petName)
                .status(PENDING)
                .photoUrls(List.of("http://test_elixir_unit_test.com"));
    }

    public PetStoreStepsDefinitions() {
        Given("^A new default pet is created$", () -> {
            ResponseEntity<Pet> petResponseEntity = petServiceSteps.create(pet);
            assertThat(petResponseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
            createdPet = petResponseEntity.getBody();
        });

        Then("^Return code is (\\d+)$", (Integer expectedReturnCode) -> {
            assertThat(statusCode).as("Return status for fetching pet").isEqualTo(expectedReturnCode);
        });

        When("^User tries to fetch a pet by id$", () -> {
            statusCode = petServiceSteps.getById(createdPet.getId()).getStatusCode().value();
        });

        When("User fetches a pet by id {long}", (Long id) -> {
            statusCode = petServiceSteps.getById(id).getStatusCode().value();
        });

        Given("^New pets with all statuses are created$", () -> {
            Stream.of(Pet.StatusEnum.values()).forEach(status -> {
                pet.setStatus(status);
                ResponseEntity<Pet> petResponseEntity = petServiceSteps.create(pet);
                softly.assertThat(petResponseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
            });
            softly.assertAll();
        });

        When("^User tries to find a pet by (-?\\w+)$", (String status) -> {
            ResponseEntity<Pet[]> petResponseEntity = petServiceSteps.findByStatus(status);

            foundPets = petResponseEntity.getBody();
            statusCode = petResponseEntity.getStatusCode().value();
        });

        And("^Number of results is equal or greater than (\\d+)$", (Integer numberOfResults) -> {
            assertThat(foundPets.length).as("Number of found pets").isGreaterThanOrEqualTo(numberOfResults);
        });

        When("New tags {string} is added to the existing pet", (String tagString) -> {
            final String[] tags = tagString.split(",");
            newTags = IntStream.range(0, tags.length).mapToObj(index -> new Tag().name(tags[index]).id(Long.valueOf(index))).collect(Collectors.toList());
            Pet updatedPet = createdPet.tags(newTags);
            ResponseEntity<Pet> petResponseEntity = petServiceSteps.update(updatedPet);
            statusCode = petResponseEntity.getStatusCode().value();
            ResponseEntity<Pet> petResponseEntityAfterUpdate = petServiceSteps.getById(updatedPet.getId());
            updatedTags = petResponseEntityAfterUpdate.getBody().getTags();
        });

        And("^Updated pet has correct tags$", () -> {
            assertThat(updatedTags).isEqualTo(newTags).as("Tags after pet update");
        });

        When("A name of the existing pet is updated with name {string} using form data", (String name) -> {
            petName = name;
            MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<String, String>();
            parametersMap.add("name", name);
            ResponseEntity<ModelApiResponse> petUpdateEntity = petServiceSteps.updateWithForm(createdPet.getId(), parametersMap);
            statusCode = petUpdateEntity.getStatusCode().value();

            ResponseEntity<Pet> updatedPetResponseEntity = petServiceSteps.getById(createdPet.getId());
            updatedPetName = updatedPetResponseEntity.getBody().getName();
        });

        And("^Updated pet has correct name$", () -> {
            assertThat(updatedPetName).as("Updated pet name").isEqualTo(petName);
        });

        When("^A pet is deleted$", () -> {
            statusCode = petServiceSteps.delete(createdPet.getId()).getStatusCode().value();
        });

        And("^Deleted pet is not found$", () -> {
            HttpStatus statusCodePetNotExists = petServiceSteps.getById(createdPet.getId()).getStatusCode();
            softly.assertThat(statusCodePetNotExists).as("Status for non existing pet").isEqualTo(HttpStatus.NOT_FOUND);
        });

        When("File {string} is uploaded", (String fileName) -> {
            ResponseEntity<ModelApiResponse> fileUploadResponse = petServiceSteps.uploadFile(createdPet.getId(), fileName);
            statusCode = fileUploadResponse.getStatusCode().value();
            message = fileUploadResponse.getBody().getMessage();
        });

        And("Response message contains correct text {string}", (String expectedText) -> {
            assertThat(message).as("File upload response message").contains(expectedText);
        });
    }
}