Feature: Modifying pets

        As a API consumer
        I want to be able to modify/update pets by ID
        So i can modify and delete pets

Acceptance Criteria
  * A single pet is updated with tags, return code - 200
  * A single pet is updated with name using form data, return code - 200
  * A single pet is deleted, return code - 200
  * A single pet is updated with file upload, return code - 200

  Background: Create predefined test data and components
    Given A new default pet is created

  Scenario: Update a single pet - add new tags
    When New tags "Tag#1,Tag#2" is added to the existing pet
    Then Return code is 200
    And Updated pet has correct tags

  Scenario: Update a single pet name with form data
    When A name of the existing pet is updated with name "Alex" using form data
    Then Return code is 200
    And Updated pet has correct name

  Scenario: Delete a single pet
    When A pet is deleted
    Then Return code is 200
    And Deleted pet is not found

  Scenario:  Upload a file for a single pet
    When File "pets.png" is uploaded
    Then Return code is 200
    And Response message contains correct text "File uploaded to ./"