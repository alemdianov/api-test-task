Feature: Fetching pets

        As a API consumer
        I want to be able to fetch pets by ID

Acceptance Criteria
  * A single pet with valid/existing ID is available, return code - 200
  * A single pet with invalid ID is not available, return code - 404

  Background: Create predefined test data and components
  Given A new default pet is created

  Scenario: It is possible to fetch a pet by ID
    When User tries to fetch a pet by id
    Then Return code is 200


  Scenario: It is impossible to fetch a pet by the wrong ID
    When User fetches a pet by id 123456789
    Then Return code is 404
