Feature: Searching pets

        As a API consumer
        I want to be able to search for pets

Acceptance Criteria
  * Pets valid status are available during search, return code - 200, number of results - 1 or more

  Background:
    Given New pets with all statuses are created

  Scenario Outline: It is possible to search for a pet by status <status>
    When User tries to find a pet by <status>
    Then Return code is 200
    And Number of results is equal or greater than 1
    Examples:
      | status    |
      | available |
      | pending   |
      | sold      |