# api-test-task

API tests for API (web service) testing

![Pet Store Logo](src/test/resources/petstore-logo.png "Logo")

### API Description

As target API a Petstore webservice was chosen as fast, well documented and opensource solution

[link to the documentaion](https://petstore.swagger.io/)

### API overview

- *pet*   - Everything about your Pets
- *user*  - Operations about user
- *store* - Access to Petstore orders

The given test suite covers pet endpoint currently.


| Method | Path | Description | 
| :---- | :----------------------- | :---------------- | 
|POST   | /pet/{petId}/uploadImage | uploads an image |
|POST   | /pet                     | Add a new pet to the store |
|PUT    | /pet                     | Update an existing pet |
|GET    | /pet/findByStatus        | Finds Pets by status |
|GET    | /pet/{petId}             | Find pet by ID |
|POST   | /pet/{petId}             | Updates a pet in the store with form data |
|DELETE | /pet/{petId}             | Deletes a pet |


### How to add a new test
###### A current setup contains next steps to interact with the endpoints :
### Implemented steps
```java
    @Step("#get all pets by id")
    public ResponseEntity<Pet> getById(Long id) {}

    @Step("#find a pet by status - {0}")
    public ResponseEntity<Pet[]> findByStatus(String status) {}


    @Step("#reate a new pet")
    public ResponseEntity<Pet> create(Pet pet) {}


    @Step("#update an existing pet")
    public ResponseEntity<Pet> update(Pet pet) {}


    @Step("#update an existing pet with form")
    public ResponseEntity<ModelApiResponse> updateWithForm(Long petId, MultiValueMap<String, String> parametersMap) {}


    @Step("#delete an existing pet by id")
    public ResponseEntity<ModelApiResponse> delete(Long petId) {}

    @Step("#upload a file to an existing pet")
    public ResponseEntity<ModelApiResponse> uploadFile(Long id, String fileName) {}
 ```

###### List of available BDD instructions to be reused:
|Predefined Step name  | BDD instructions                                                          |    
|:---------------------| :------------------------------------------------------------------------ |    
| Given                |"^A new default pet is created$"                                           |    
| Then                 |"^Return code is (\\d+)$"                                                  |    
| When                 |"^User tries to fetch a pet by id$"                                        |    
| When                 |"User fetches a pet by id {long}"                                          |    
| Given                |"^New pets with all statuses are created$"                                 |    
| When                 |"^User tries to find a pet by (-?\\w+)$"                                   |    
| And                  |"^Number of results is equal or greater than (\\d+)$"                      |    
| When                 |"New tags {string} is added to the existing pet"                           |    
| And                  |"^Updated pet has correct tags$"                                           |    
| When                 |"A name of the existing pet is updated with name {string} using form data" |    
| And                  |"^Updated pet has correct name$"                                           |    
| When                 |"^A pet is deleted$"                                                       |    
| And                  |"^Deleted pet is not found$"                                               |    
| When                 |"File {string} is uploaded"                                                |    
| And                  |"Response message contains correct text {string}"                          |    


To add a new test or scenario you can reuse existing instructions, and update or extend existing features.
To add a new suite/runner file/configuration you need to add a new file with name pattern _**/*TestSuite.java_ and minimum content:

```java
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"com.alemdianov.cucumber"}
)
public class SerenityTestSuite {
}

```


### How to run

###### To run tests run the next command

```java
.mvnw clean verify
```

###### or

```java
mvnw.cmd clean test
```

### How to generate and run HTML report

##### HTML report are generated automatically during test run and available in the location: target/site/serenity  

### To see build on GitLab CI/CD

[link to pipeline](https://gitlab.com/alemdianov/api-test-task/-/pipelines)

### To open HTML report online

[link to Report](https://alemdianov.gitlab.io/api-test-task)

### Next steps
- [ ] Implement steps for *store* and *user* endpoints
- [ ] Add BDD steps for that endpoints
- [ ] Add dynamic mappings to  BDD steps to make them more universal 
- [ ] Add live documentation
    - [ ] Requirements
    - [ ] Themes
    - [ ] Capabilities
    - [ ] Features